class JobApplicationsController < ApplicationController
  before_action :set_job_application, only: [:show, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource

  # GET /job_applications
  def index
    if current_user.admin?
      @job_applications = JobApplication.joins(:job).where(jobs: {user: current_user}).all
    else
      @job_applications = JobApplication.where(user: current_user).all
    end    

    render json: @job_applications
  end

  # GET /job_applications/1
  def show
    @job_application.status = JobApplication.statuses[:seen]
    @job_application.save
    render json: @job_application
  end

  # POST /job_applications
  def create
    params[:job_application][:user_id] = current_user.id

    @job_application = JobApplication.new(job_application_params)

    if @job_application.save
      render json: @job_application, status: :created, location: @job_application
    else
      render json: @job_application.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /job_applications/1
  def update
    params[:job_application][:user_id] = current_user.id

    if @job_application.update(job_application_params)
      render json: @job_application
    else
      render json: @job_application.errors, status: :unprocessable_entity
    end
  end

  # DELETE /job_applications/1
  def destroy
    @job_application.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_application
      @job_application = JobApplication.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def job_application_params
      params.require(:job_application).permit(:status, :user_id, :job_id)
    end
end
