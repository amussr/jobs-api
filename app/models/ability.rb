class Ability
  include CanCan::Ability

  def initialize(user)  
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can [:index, :show, :create], Job
      can [:update, :destroy], Job, user_id: user.id

      cannot [:create, :update, :destroy], JobApplication
      can [:index], JobApplication
      can [:show], JobApplication, job: {user_id: user.id}
    else
      can [:index, :show], Job

      can [:index, :create], JobApplication
      can [:show, :update, :destroy], JobApplication, user_id: user.id
    end
  end
end
