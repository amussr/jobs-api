class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions
  include DeviseTokenAuth::Concerns::SetUserByToken
  check_authorization :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    render json: { :error => exception.message }, :status => 403
  end
end
