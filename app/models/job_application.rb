class JobApplication < ApplicationRecord
  belongs_to :user
  belongs_to :job

  enum status: { not_seen: 1, seen: 2 }
end
