FROM ruby:2.4.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /job-board
WORKDIR /job-board
ADD Gemfile /job-board/Gemfile
ADD Gemfile.lock /job-board/Gemfile.lock
RUN bundle install
ADD . /job-board