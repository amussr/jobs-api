class RegistrationsController < DeviseTokenAuth::RegistrationsController

  private

  def sign_up_params
    params.permit(*params_for_resource(:sign_up) << :role)
  end

  def account_update_params
    params.permit(*params_for_resource(:account_update) << :role)
  end
end